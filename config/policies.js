/**
 * Policy Mappings
 * (sails.config.policies)
 *
 * Policies are simple functions which run **before** your actions.
 *
 * For more information on configuring policies, check out:
 * https://sailsjs.com/docs/concepts/policies
 */

module.exports.policies = {

	/***************************************************************************
	 *                                                                          *
	 * Default policy for all controllers and actions, unless overridden.       *
	 * (`true` allows public access)                                            *
	 *                                                                          *
	 ***************************************************************************/

	'*': true,
	'security/grant-csrf-token': ['authenticated', "refreshSessionCookie"],
	'auth': true,
	'index': {
		'*': ["authenticated", "refreshSessionCookie"]
	},
	'admin/airportlist': {
		'*': ["authenticated", "isAdmin", "refreshSessionCookie"],
		'addairports': false
	},
	'admin/user': {
		'*': ["authenticated", "isAdmin"]
	},
	'userattendance': {
		'*': ["authenticated", "isAdmin", "refreshSessionCookie"]
	},
	'customer_bill': {
		'*': ["authenticated", "isAdmin", "refreshSessionCookie"]
	},
	'Enquiry': {
		'*': ["authenticated"]
	},
	'Mobileapp': { '*': ["authenticated", "refreshSessionCookie"]},
	
};