/**
 * Built-in Log Configuration
 * (sails.config.log)
 *
 * Configure the log level for your app, as well as the transport
 * (Underneath the covers, Sails uses Winston for logging, which
 * allows for some pretty neat custom transports/adapters for log messages)
 *
 * For more information on the Sails logger, check out:
 * https://sailsjs.com/docs/concepts/logging
 */

var winston = require('winston');
require('winston-daily-rotate-file');

const customLogger = winston.createLogger({
	level: 'silly',
	//format: winston.format.json(),
	transports: [
		new winston.transports.DailyRotateFile({
			name: 'file',
			datePattern: 'YYYY-MMM-DD',
			filename: 'log/idos_log_silly_',
			level: 'silly'
		}),
		/*new winston.transports.DailyRotateFile({
			name: 'file',
			datePattern: 'YYYY-MMM-DD',
			filename: 'log/idos_log_info_',
			level: 'info'
		})*/
	],
	exitOnError: false
});




module.exports.log = {
	// Pass in our custom logger, and pass all log levels through.
	//custom: customLogger,
	// Disable captain's log so it doesn't prefix or stringify our meta data.
	inspect: false
};