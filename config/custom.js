/**
 * Custom configuration
 * (sails.config.custom)
 *
 * One-off settings specific to your application.
 *
 * For more information on custom configuration, visit:
 * https://sailsjs.com/config/custom
 */

module.exports.custom = {

	/***************************************************************************
	 *                                                                          *
	 * Any other custom config this Sails app should use during development.    *
	 *                                                                          *
	 ***************************************************************************/
	// mailgunDomain: 'transactional-mail.example.com',
	// mailgunSecret: 'key-testkeyb183848139913858e8abd9a3',
	// stripeSecret: 'sk_test_Zzd814nldl91104qor5911gjald',
	// …
	department_name: ["department 1", "department 2", "department 3","department 4"],
	month: ["01", "02","03","04","05","06","07","08","09","10","11","12"],
	nature_of_expense: ["1st reason", "2nd reason", "3rd reason", "4th reason"],
	Expense_departments: ["department 1", "department 2", "department 3", "department 4"],
	nature_expense: ["nature 1", "nature 2", "nature 3", "other"],
	city_list: [
		{
			"Div": "A",
			"City": {
				"Delhi": {
					"store1": [
						"28.7190",
						"77.1125"
					],
					"store2": [
						"28.5672",
						"77.3211"
					],
					"store3": [
						"28.6425",
						"77.1063"
					],
					"store4": [
						"28.6478",
						"77.1101"
					],
					"store5": [
						"28.6678",
						"77.6608"
					],
					"store6": [
						"28.6678",
						"77.6708"
					]
				},
				"Mumbai": {
					"store1": [
						"19.0760",
						"72.8777"
					],
					"store2": [
						"19.5672",
						"72.3211"
					],
					"store3": [
						"19.6425",
						"72.1063"
					],
					"store4": [
						"19.6478",
						"72.1101"
					],
					"store5": [
						"19.6678",
						"72.6608"
					],
					"store6": [
						"19.6678",
						"72.6708"
					]
				},
				"Kolkata": {
					"store1": [
						"22.5726",
						"88.3639"
					],
					"store2": [
						"22.5672",
						"88.3211"
					],
					"store3": [
						"22.6425",
						"88.1063"
					],
					"store4": [
						"22.6478",
						"88.1101"
					],
					"store5": [
						"22.6678",
						"88.6608"
					],
					"store6": [
						"22.6678",
						"88.6708"
					]
				}
			}
		}
	],
	
	div : ["A","B","C","D"],
	city_A :["Delhi","Mumbai","Kolkata"],
	city_B : ["Kolhapur","Nashik","sangali"],
	city_C: ["belgum","Gokak","Pune"]
};