/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {
	//  ╦ ╦╔═╗╔╗ ╔═╗╔═╗╔═╗╔═╗╔═╗
	//  ║║║║╣ ╠╩╗╠═╝╠═╣║ ╦║╣ ╚═╗
	//  ╚╩╝╚═╝╚═╝╩  ╩ ╩╚═╝╚═╝╚═╝

	/***************************************************************************
	 *                                                                          *
	 * Make the view located at `views/homepage.ejs` your home page.            *
	 *                                                                          *
	 * (Alternatively, remove this and add an `index.html` file in your         *
	 * `assets` directory)                                                      *
	 *                                                                          *
	 ***************************************************************************/
	"GET /csrfToken": {
		action: "security/grant-csrf-token"
	},
	"GET /": "IndexController.index",
	"GET /imlost": {
		view: "pages/imlost"
	},
	"/storelatitude": {
		view: "pages/storelatitude"
	},
	"GET /index": "IndexController.index",
	//"POST /saveconsigneegstin": "admin/ConsigneesController.saveconsigneegstin",
	// routes for inward-cargo
	//'GET /constants': 'admin/ConstantsController.userconstant',
	// "GET /constants": "admin/ConstantsController.getconstantlist",
	// "POST /constants": "admin/ConstantsController.constants",
	//"GET /consignees": "admin/ConsigneesController.getconsigneeslist",
	//"POST /consignees": "admin/ConsigneesController.consignees",
	//"GET /exchangerates": "admin/ExchangeRatesListController.getexchangerateslist",
	//"POST /exchangerates": "BudgetAnalyzerController.exchangerateslist",
	// 'POST /exchangerates': 'admin/ExchangeRatesListController.exchangerateslist',
	//"POST /deleteexchangerates": "admin/ExchangeRatesListController.deleteexchangerates",
	//"GET /reasons": "admin/ReasonsController.getreasonslist",
	//"POST /reasons": "admin/ReasonsController.reasons",
	//"POST /deletereason": "admin/ReasonsController.deletereason",
	// "GET /airportlist": {
	// 	view: 'pages/blank-page'
	// },
	
	"GET /getcustomerbill": "Customer_billController.getcustomerbilllist",
	"POST /deletecustomerbilllist": "UserattendanceController.deletecustomerbilllist",
	
	"GET /userattendance": "UserattendanceController.getuserattendancelist",
	"POST /deleteuserlist": "UserattendanceController.deleteuserlist",
	
	
	"GET /getenquirylist": "EnquiryController.getuserenquirylist",
	"POST /deleteenquiry": "EnquiryController.deleteenquiry",
	
	//mobile server API's
	"POST /createuser": "MobileappController.createuser",
	"POST /customerdetail": "MobileappController.customerdetail",
	"POST /saveattendance": "MobileappController.saveattendance",
	"POST /searchphoneno": "MobileappController.searchphoneno",
	
	
	
	// "POST /airportlist": "admin/AirportListController.airportlist",
	// "POST /deleteairport": "admin/AirportListController.deleteairport",
	// "GET /addairports": "admin/AirportListController.addairports",
	// "GET /gstcodes": "admin/GstCodesController.getgstcodes",
	// "POST /gstcodes": "admin/GstCodesController.gstcodes",
	// "POST /deletegstcodes": "admin/GstCodesController.deletegstcodes",
	
	// "GET /getvendorlist": "VendorController.getvendorlist",
	// "POST /createvendor": "VendorController.createvendor",
	// "POST /deletevendor": "VendorController.deletevendor",
	
	// "POST /createbudget": "BudgetAnalyzerController.createbudget",
	// "GET /getbudgetlist": "BudgetAnalyzerController.getbudgetlist",
	// "POST /deletebudgetlist": "BudgetAnalyzerController.deletebudgetlist",
	
	// "GET /getexpenselist": "ExpenseLoggerController.getexpenselist",
	// "POST /createexpenselog": "ExpenseLoggerController.createexpenselog",
	// "POST /deleteexpense": "ExpenseLoggerController.deleteexpense",
	// "GET /add": "ExpenseLoggerController.add",
	
	// "GET /getsectorlist": "SectorController.getsectorlist",
	// "POST /createsector": "SectorController.createsector",
	// "POST /deletesector": "SectorController.deletesector",
	

	// routes for theme
	'/blank-page': {view: 'pages/blank-page'},
	//'/index': {view: 'pages/index'},
	//'/index2': {view: 'pages/index2'},
	//'/': {view: 'pages/index'},
	//'/authentication-login': {view: 'pages/authentication-login'},

	"GET /register": "admin/UserController.registeruser",
	"POST /register": "admin/UserController.adduser",
	"POST /deleteuser": "admin/UserController.deleteuser",
	//"POST /changepassword": "admin/UserController.changepassword",

	//'/blank': {view: 'pages/blank'}
	"/error-403": {
		view: "pages/error-403"
	},
	"/error-404": {
		view: "pages/error-404"
	},
	"/error-405": {
		view: "pages/error-405"
	},
	"/error-500": {
		view: "pages/error-500"
	},
	'/charts': {view: 'pages/charts'},
	'/form-basic': {view: 'pages/form-basic'},
	'/form-wizard': {view: 'pages/form-wizard'},
	'/grid': {view: 'pages/grid'},
	'/icon-fontawesome': {view: 'pages/icon-fontawesome'},
	'/icon-material': {view: 'pages/icon-material'},
	'/pages-buttons': {view: 'pages/pages-buttons'},
	'/pages-invoice': {view: 'pages/pages-invoice'},
	'/pages-elements': {view: 'pages/pages-elements'},
	'/pages-calendar': {view: 'pages/pages-calendar'},
	'/pages-chat': {view: 'pages/pages-chat'},
	'/pages-gallery': {view: 'pages/pages-gallery'},
	"GET  /login": "AuthController.getlogin",
	"POST /login": "AuthController.login",
	// 'GET  /loginldap': 'AuthController.getloginldap',
	// 'POST /loginldap': 'AuthController.loginldap',
	"/logout": "AuthController.logout",
	'/tables': {view: 'pages/tables'},
	'/widgets': {view: 'pages/widgets'},
	"/trialdropbox": function (req, res, next) {
		require("isomorphic-fetch"); // or another library of choice.
		var Dropbox = require("dropbox").Dropbox;
		var fs = require("fs");
		var path = require("path");

		var dbx = new Dropbox({
			accessToken:
				/*result.accessToken*/
				"UIF_-TeEa3UAAAAAAAAAH8E9VjsXVMwLewZyF30hBXxmGKio-L3kQaRzdyt-fHJW"
		});

		fs.readFile(path.join(__dirname, "/log.js"), "utf8", function (
			err,
			contents
		) {
			if (err) {
				console.log("Error: ", err);
			}

			// This uploads basic.js to the root of your dropbox
			dbx.filesUpload({
					path: "/log.js",
					contents: contents
				})
				.then(function (response) {
					res.send(response);
				})
				.catch(function (err) {
					res.send(err);
				});
		});
		// var prompt = require('prompt');

		// prompt.start();
		//
		// prompt.get({
		//   properties: {
		//     accessToken: {
		//       description: 'Please enter an API V2 access token'
		//     }
		//   }
		// }, function (error, result) {
		//
		// });
	}
	/***************************************************************************
	 *                                                                          *
	 * More custom routes here...                                               *
	 * (See https://sailsjs.com/config/routes for examples.)                    *
	 *                                                                          *
	 * If a request to a URL doesn't match any of the routes in this file, it   *
	 * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
	 * not match any of those, it is matched against static assets.             *
	 *                                                                          *
	 ***************************************************************************/

	//  ╔═╗╔═╗╦  ╔═╗╔╗╔╔╦╗╔═╗╔═╗╦╔╗╔╔╦╗╔═╗
	//  ╠═╣╠═╝║  ║╣ ║║║ ║║╠═╝║ ║║║║║ ║ ╚═╗
	//  ╩ ╩╩  ╩  ╚═╝╝╚╝═╩╝╩  ╚═╝╩╝╚╝ ╩ ╚═╝

	//  ╦ ╦╔═╗╔╗ ╦ ╦╔═╗╔═╗╦╔═╔═╗
	//  ║║║║╣ ╠╩╗╠═╣║ ║║ ║╠╩╗╚═╗
	//  ╚╩╝╚═╝╚═╝╩ ╩╚═╝╚═╝╩ ╩╚═╝

	//  ╔╦╗╦╔═╗╔═╗
	//  ║║║║╚═╗║
	//  ╩ ╩╩╚═╝╚═╝
};