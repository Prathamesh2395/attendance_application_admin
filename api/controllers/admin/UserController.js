/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
	adduser: function (req, res) {
		var name = req.body.attendance_registeruser_username;
		var phone_no = req.body.attendance_registeruser_phone;
		// var password = req.body.inwardcargo_registeruser_password;
		// var iata_code = req.body.inwardcargo_registeruser_city;
		var role = req.body.attendance_registeruser_role;

		if (name == undefined || name == null || name == '') {
			sails.log.error(req.user.username + ' - ' + new Date() + ' ERR - (adduser - post) User Name cannot be blank');
			return res.view('pages/imlost', {
				error: 'User Name cannot be blank'
			});
		} else if (phone_no == undefined || phone_no == null || phone_no == '') {
			sails.log.error(req.user.username + ' - ' + new Date() + ' ERR - (adduser - post) phone_no cannot be blank');
			return res.view('pages/imlost', {
				error: 'phone_no cannot be blank'
			});
			/*} else if (password == undefined || password == null || password == '') {
				sails.log.error(req.user.username + ' - ' + new Date() +' ERR - (adduser - post) Password cannot be blank');
				return res.view('pages/imlost', {error: 'Password cannot be blank'});
			*/
		} else if ((phone_no + "").length!=10) {
		return res.view('pages/imlost', {
			error: 'phone_no must be of 10 digit'
		});
		} else {
			User.findOrCreate({
					username: name
				}, {
					role: role,
					username: name,
					phone: phone_no,
				})
				.exec(async (err, user, wasCreated) => {
					if (err) {
						sails.log.error(req.user.username + ' - ' + new Date() + ' ERR - (adduser - post)' + err);
						return res.view('pages/imlost', {
							error: 'Something went wrong while finding or creating user'
						});
					}
					if (wasCreated) {
						sails.log.info(req.user.username + ' - ' + new Date() + ' INFO - (adduser - post) render register if newly created user');
						return res.redirect('/register');
					} else {
						User.update({
								username: user.username
							}, {
								role: role,
								username: name,
								phone: phone_no
							}).fetch()
							.exec(function (err, updatedUser) {
								if (err) {
									sails.log.error(req.user.username + ' - ' + new Date() + ' ERR - (adduser - post) ' + err);
									return res.view('pages/imlost', {
										error: 'Something Happens During Updating'
									});
								} else {
									sails.log.info(req.user.username + ' - ' + new Date() + ' INFO - (adduser - post) User updated successfully');
									return res.redirect('/register');
								}
							});
					}
				});
		}
	},
	registeruser: function (req, res) {
				User.find({
					where: {}
				}, function (err, users) {
					if (err) {
						sails.log.error(req.user.username + ' - ' + new Date() + ' ERR - (registeruser - get)' + err);
						return res.view('pages/imlost', {
							error: err
						});
					} else {
						sails.log.info(req.user.username + ' - ' + new Date() + ' INFO - (registeruser - get) render adduser page');
						return res.view('pages/adduser', {
							userlist: users
						});
					}
				});		
	},
	deleteuser: function (req, res) {
		var deleteUserId = req.body.attendance_user_deleteid;
		User.destroy({
			'_id': deleteUserId
		}).exec(function (err, user) {
			if (err) {
				sails.log.error(req.user.username + ' - ' + new Date() + ' ERR - (deleteuser - post) ' + err);
				return res.view('pages/imlost', {
					error: 'Error while deleting user'
				});
			} else {
				sails.log.info(req.user.username + ' - ' + new Date() + ' INFO - (deleteuser - post) User deleted successfully');
				return res.send({
					result: true
				});
			}
		});
	},
	
};