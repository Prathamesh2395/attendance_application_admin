const passport = require('passport');
const voca = require('voca');

module.exports = {

	getlogin: function (req, res) {
		if (req.user) {
			if (req.user.role === 'user') {
				sails.config.globals.putinfolog(req.user.username, req.options.action, 'get', '1');
				res.redirect('/');
			} else if (req.user.role === 'admin') {
				sails.config.globals.putinfolog(req.user.username, req.options.action, 'get', '2');
				res.redirect('/userattendance');
			} else {
				sails.log.error(' - ' + new Date() + ' ERR - (getlogin - get)' + 'userrole may be the reason behind failure');
				res.view('pages/authentication-login');
			}
		} else {
			res.view('pages/authentication-login');
		}
	},
	getloginldap: function (req, res) {
		if (req.user) {
			if (req.user.role === 'user') {
				sails.config.globals.putinfolog(req.user.username, req.options.action, 'get', '1');
				res.redirect('/login');
			} else if (req.user.role === 'admin') {
				sails.config.globals.putinfolog(req.user.username, req.options.action, 'get', '2');
				res.redirect('/userattendance');
			} else {
				sails.log.error(' - ' + new Date() + ' ERR - (getloginldap - get)' + 'userrole may be the reason behind failure');
				res.view('pages/authentication-login-ldap');
			}
		} else {
			//sails.log.error(' - ' + new Date() +' ERR - (getloginldap - get)' + 'May be user not found');
			res.view('pages/authentication-login-ldap');
		}
	},

	login: function (req, res) {
		passport.authenticate('local', function (err, user, info) {
			if ((err) || (!user)) {
				sails.log.error(' - ' + new Date() + ' ERR - (login - post)' + err);
				return res.redirect('/login');
				//return res.view('pages/authentication-login',{message: info.message});
				//res.view('/page-login', {message: info.message, layout: null});
				//return res.send({message: info.message,user});
			} else {
				req.logIn(user, function (err) {
					if (err) {
						sails.log.error(' - ' + new Date() + ' ERR - (login - post)' + err);
						res.send(err);
					} else {
						sails.log.info(user.username + ' - ' + new Date() + ' - User logged in');
						return res.redirect('/userattendance');
					}
				});
			}
		})(req, res);
	},

	loginldap: function (req, res) {
		passport.authenticate('ldapauth', function (err, user, info) {
			if (err) {
				sails.log.error(' - ' + new Date() + ' ERR - (loginldap - post)' + err);
				return res.view('pages/authentication-login-ldap', {
					info: info
				});
			}

			if (user) {

				/////////////////////////////////	HACK

				if (user.employeeType === 'AppAdmin')
					user.employeeType = 'admin';

				if (user.employeeType === 'User')
					user.employeeType = 'user';

				/////////////////////////////////	HACK

				if (user.division && (user.employeeType === 'admin' || user.employeeType === 'user')) {
					user.username = user.sAMAccountName;
					user.role = user.employeeType;
					user.iata_code = user.division.split(',');
					req.logIn(user, function (err) {
						if (err) {
							sails.log.error(' - ' + new Date() + ' ERR - (loginldap - post)' + err);
							return next(err);
						}
						sails.log.info(user.username + ' - ' + new Date() + ' - User logged in');
						return res.redirect('/userattendance');
					});
				} else {
					sails.log.error(' - ' + new Date() + ' ERR - (loginldap - post)' + 'You are not authorized for access');
					return res.view('pages/authentication-login-ldap', {
						info: 'You are not authorized for access'
					});
				}
			} else {
				info = voca.replaceAll(info, sails.config.globals.ldap_access_url, '***.***.***.***');
				info = voca.replaceAll(info, sails.config.globals.ldap_access_port, '****');
				sails.log.error(' - ' + new Date() + ' ERR - (loginldap - post)' + info);
				return res.view('pages/authentication-login-ldap', {
					info: info
				});
			}
		})(req, res);
	},

	logout: function (req, res) {
		req.logout();
		res.redirect('/login');
	}
};