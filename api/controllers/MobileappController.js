/**

* UserController

*

* @description :: Server-side actions for handling incoming requests.

* @help :: See https://sailsjs.com/docs/concepts/actions

*/



module.exports = {

	creatuser: async function (req, res) {
		var user_name = req.body.user_name;
		var user_email = req.body.user_email;
		var user_phone_no = req.body.user_phone_no;
		//var user_address = req.body.user_address;
		//var user_questions = req.body.user_questions;
		var sku_number = req.body.product_sku;
		var not_purchase_reason = req.body.not_purchase_reason;
		var user_product = req.body.user_product;
		Enquiry.create({
			name: user_name,
			email: user_email,
			phone_no: user_phone_no,
			//address: user_address,
			sku_number: sku_number,
			not_purchase_reason: not_purchase_reason,
			//questions: user_questions,
			product: user_product
		}).exec(function (err, customer) {
			if (err) {
				return res.send("something wrong in saving data");
			} else {
				return res.send({ user_data: user_data });
			}
		});
	},

	customerdetail: async function (req, res) {
		//console.log(req.body);
		var customer_name = req.body.customer_name;
		var customer_product = req.body.customer_product;
		//var customer_no = req.body.customer_no;
		var user_mobile_no = req.body.user_mobile_no;
		var customer_payment_method = req.body.customer_payment_method;
		var sku_number = req.body.sku_no;
		var customer_activate_warranty = req.body.customer_activate_warranty;
		var customer_phone_no = req.body.customer_phone_no;
		var customer_email_address = req.body.customer_email_address;
		var customer_billno = req.body.customer_billno;
		var customer_billamount = req.body.customer_billamount;
		var customer_billimage = req.body.customer_billimage;
		//var customer_billimage_name = req.body.customer_billimage_name;
		console.log(req.body);
		var fs = require('fs');
		var shell = require('shelljs');
		var bodyParser = require("body-parser");
		console.log("\n\n\n\n\n" + customer_billimage);
		var buf = new Buffer(customer_billimage, 'base64', function (buff) {
			//console.log(buff);
		});
		var date = new Date();
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		var timestamp = date.getTime();
		var ext = ".jpeg";
		var path = "./" + year + "_bill/" + month + "/" + day + "/";
		console.log(path);
		shell.mkdir("-p", path);
		fs.writeFile(path + customer_phone_no + "_" + timestamp + ext, buf, function (err) {
			//console.log(err);
		});
		Customer.create({
			name: customer_name,
			createdby: user_mobile_no,
			product: customer_product,
			//customer_no: customer_no,
			payment_method: customer_payment_method,
			sku_number: sku_number,
			activate_warranty: customer_activate_warranty,
			phone_no: customer_phone_no,
			email_address: customer_email_address,
			billno: customer_billno,
			billamount: customer_billamount,
			billimage: path + customer_phone_no + "_" + timestamp + ext,
		}).exec(function (err, customer) {
			if (err) {
				return res.send(false);
			} else {
				return res.send({ customer: customer });
			}
		});

		// console.log(customer);

		// if (customer) {

		// 	return res.send(customer);

		// } else {

		// 	return res.send("something wrong in saving data");

		// }

	},





	// userlogin: async function (req, res) {
	// 	var user_login_phone = req.body.login_phone;
	// 	var user_login_name = req.body.login_name;
	// 	var login_data = await User_login.create({
	// 		name: user_login_name,
	// 		phone_no: user_login_phone
	// 	}).fetch();
	// 	if (login_data) {

	// 		return res.send(login_data);

	// 	} else {

	// 		return res.send("something wrong in saving data");

	// 	}
	// },



	saveattendance: async function (req, res) {
		var attendance_timestamp = req.body.timestamp;
		var attendance_phone_no = req.body.phone_no;
		var attendance_name = req.body.name;
		var attendance_image = req.body.image;
		var attendance_latitude = req.body.latitude;
		var attendance_longitude = req.body.longitude;
		var date = new Date();
		var year = date.getFullYear();
		var month = date.getMonth() + 1;
		var day = date.getDate();
		var timestamp = date.getTime();
		var ext = ".jpeg";
		var shell = require('shelljs');
		var fs = require('fs');
		//var ext = ".jpeg";
		var buf = new Buffer(attendance_image, 'base64', function (buff) {
			//console.log(buff);
			var path = "./" + year + "_attendance/" + month + "/" + day + "/";
			console.log(path);
			shell.mkdir("-p", path);
			fs.writeFile(path + attendance_phone_no + "_" + timestamp + ext, buf, function (err, path) {
				if (err) {
					res.send("error in saving image");
				}
			});
			
			Attendance.create({
				timestamp: attendance_timestamp,
				phone_no: attendance_phone_no,
				name: attendance_name,
				image: path + attendance_phone_no + "_" + timestamp + ext,
				latitude: attendance_latitude,
				longitude: attendance_longitude
			}).exec(function (err, attendance) {
				if (err) {
					return res.send(false);
				} else {
					return res.send(true);
				}
			})
		});
		

		

		// var attendence_data = await Attendance.create({

		// 	timestamp: attendance_timestamp,

		// 	phone_no: attendance_phone_no,

		// 	name: attendance_name,

		// 	image: path + attendance_phone_no + "_" + timestamp + ext,

		// 	latitude: attendance_latitude,

		// 	longitude: attendance_longitude

		// }).fetch();

		//console.log(attendence_data);

		// if (fs.existsSync(path)) {

		//   req.file('image').upload({

		//     dirname: '../../image/user/'

		//   }, function (err, files) {

		//     sails.log.debug('file is :: ', +files);

		//     maxBytes: 10000000;

		//     if (err) return res.serverError(err);

		//     console.log(files);



		//   })

		// }

		// if (attendence_data) {

		// 	return res.send(true);

		// } else {

		// 	return res.send(false);

		// }

	},
	searchphoneno: async function (req, res) {
		var phone = req.body.phone;
		console.log(phone);
		// var isPhone = await User.find({
		// 	where: {
		// 		phone: phone_no
		// 	},
		// });
		// console.log(isPhone[0]);
		// if (isPhone[0]) {
		// 	return res.send(true);
		// } else {
		// 	return res.send(
		// 		false
		// 	);

		// }
		User.find({
			where: {
				phone: phone
			},
		}, function (err, user) {
			if (err) {
				sails.config.globals.putinfolog(req.user.username, req.options.action, req.method, err);
				return res.send(false);
			} else {
				sails.config.globals.putinfolog(req.user.username, req.options.action, req.method, "customers detail found and send successfully");
				//return res.send(true);
				return res.send({ userdetail: user });
			}
		});
	}
};