/**
 * allow any authenticated user
 */

module.exports = function (req, res, ok) {
	if (req.user.role === 'admin') {
		return ok();
		//res.redirect('/');
	} else {
		return res.redirect('/loginldap');
		//return res.view('pages/page-login',{message: 'User Not Logged In'});
	}
};