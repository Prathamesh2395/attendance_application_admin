module.exports = {
	attributes: {
		name: {
			type: 'string',
		},
		email: {
			type: 'string',
		},
		phone: {
			type: 'number',
		},
		// address: {
		//   type: 'string'
		// },
		// question: {
		//   type: 'string'
		// },
		sku_number: {
			type: 'string'
		},
		not_purchase_reason: {
			type: 'string'
		},
		product: {
			type: 'string'
		},
	},
};