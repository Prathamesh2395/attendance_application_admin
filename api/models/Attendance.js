module.exports = {
	attributes: {
		timestamp: {
			type: 'number',
		},
		phone_no: {
			type: 'number',
		},
		name: {
			type: 'string',
		},
		image: {
			type: 'string',
		},
		latitude: {
			type: 'string',
		},
		longitude: {
			type: 'string'
		}
	},
};