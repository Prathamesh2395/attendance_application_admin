/**
 * ExchangeRates.js
 *
 * @description :: A model definition.  Represents a database table/collection/etc.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

	attributes: {
		name: {
			type: 'string',
			required: true
		},
		product: {
			type: 'string',
		},
		phone_no: {
			type: 'number',
			required: true
		},
		address: {
			type: 'string',
		},
		bill_no: {
			type: 'string',
		},
		bill_amount: {
			type: 'number',
		},
		bill_image:{
			type: 'string',
		}
	},
};