module.exports = {

	friendlyName: 'Email Validations',

	description: 'Validate each email entered by user',

	inputs: {
		//array of email id's which we have to validate
		emailids: {
			type: 'ref'
		}
	},

	exits: {
		success: {
			outputDescription: "Email's are valid .",
		}
	},
	//function check's for each email id if we found invalid email then we return json with index of invalid email and status true if success and false on failure.
	fn: function (inputs, exits) {
		var isemail = {};
		isemail.status = true;
		_.forEachRight(inputs.emailids, (value, key) => {
			if (!sails.config.globals.validator.isEmail(value)) {
				isemail.status = sails.config.globals.validator.isEmail(value);
				if (inputs.emailids.length > 0)
					isemail.key = key;
			}
		});
		return exits.success(isemail);
	}
};