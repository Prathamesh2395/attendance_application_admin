module.exports = {


	friendlyName: 'Send email',


	description: '',


	inputs: {
		to: {
			type: 'string'
		},
		cc: {
			type: 'string'
		},
		bcc: {
			type: 'string'
		},

		subject: {
			type: 'string'
		},

		body: {
			type: 'string'
		},
		html: {
			type: 'string'
		},

		attachment: {
			type: 'string'
		}
	},


	exits: {

	},


	fn: function (inputs, exits) {

		//	Disabling the EMAIL since we do not have the email account via which the sending of email should happen
		/*console.log('Email is disabled as there is no email account linked')
		exits.success(true);
		return;*/

		const nodemailer = require('nodemailer');

		// Generate test SMTP service account from ethereal.email
		// Only needed if you don't have a real mail account for testing
		nodemailer.createTestAccount((err, account) => {
			// create reusable transporter object using the default SMTP transport

			let transporter = nodemailer.createTransport({
				host: 'smtp.office365.com',
				port: 587,
				secure: false, // true for 465, false for other ports
				auth: {
					user: "imports@avscargo.com", // generated ethereal user
					pass: "Roz32090" // generated ethereal passwod
				},
				requireTLS: true
			});
			// setup email data with unicode symbols
			let mailOptions = {};
			mailOptions.from = 'imports@avscargo.com'; // sender address

			if (inputs.to)
				mailOptions.to = inputs.to; // list of receivers
			if (inputs.subject)
				mailOptions.subject = inputs.subject; // Subject line
			if (inputs.body)
				mailOptions.text = inputs.body; // plain text body
			if (inputs.html)
				mailOptions.html = inputs.html; // html body
			if (inputs.attachment)
				mailOptions.attachments = [{
					filename: inputs.attachment.substr(inputs.attachment.indexOf('/') + 1),
					path: inputs.attachment,
					contentType: 'application/pdf'
				}];
			// send mail with defined transport object
			transporter.sendMail(mailOptions, (error, info) => {
				if (error) {
					sails.log.error(' - ' + new Date() + ' ERR - (send-email - helper)' + error);
					exits.success(false);
				} else {
					exits.success(true);
				}
			});
		});
	}
};